import torch
import torch.nn as nn
from torchvision import transforms
import torch.optim as optim

from dataloader.dataloader import Heart2DSegmentationDataset
# from dataloader.transforms import ToTensor
from nnet.heart2DSegmenter import Heart2DSegmenter
from nnet.unet import Unet


def main():
    # Hyperparameter
    batch_size = 1
    epochs = 1
    threshold = 0.5
    validation_size = 0.2

    learning_rate = 0.01
    momentum = 0.99

    use_gpu_count = 2

    all_devices = list(range(0, torch.cuda.device_count()))

    # temporary solution, don't use more than three because of crashes
    all_devices = all_devices[:3]

    devices = all_devices[:use_gpu_count]
    
    # Other Parameter
    to_learn = 'ENDO'  # set it to ENDO or EPI
    dataset_path = 'data/heart_scans/'
    #dataset_path = 'data/test_tripple/'


    data_transforms = transforms.Compose([transforms.RandomCrop(320, pad_if_needed=True) ,transforms.ToTensor()])
    dataset = Heart2DSegmentationDataset(dataset_path, to_learn, data_transforms)

    # dataset sizes TODO: set for cropping before
    channels = 1
    height = 320
    width = 320


    unet = Unet(channels, height, width)
    criterion = nn.MSELoss()

    # implement dataparallel when gpu should be used
    if(use_gpu_count > 0 and torch.cuda.device_count() > 0):
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if torch.cuda.device_count() > 1:
            print("Let's use", len(devices), "GPUs!")
            unet = nn.DataParallel(unet, device_ids=devices)
            criterion = nn.DataParallel(criterion, device_ids=devices)

        unet.to(device)
        criterion.to(device)
    else:
        print("Let's use CPU only!")

    
    optimizer = optim.SGD(unet.parameters(), learning_rate, momentum)
    
    segmenter = Heart2DSegmenter(unet, dataset)

    segmenter.train(epochs, criterion, optimizer, threshold, batch_size, validation_size)

    segmenter.validate(threshold)

if __name__ == "__main__":
    main()
    
    
