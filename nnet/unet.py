import torch
import torch.nn as nn
import torch.nn.functional as torchFunc

class Step(nn.Module):
    """Class that wrappes a convolution, batchnorm and relu together."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride):
        super(Step, self).__init__()
        self.conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, padding=padding, stride=stride)
        self.batchNorm = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU()

    def forward(self, batch):
        batch = self.conv(batch)
        batch = self.batchNorm(batch)
        batch = self.relu(batch)

        return batch
        
class DownStep(nn.Module):
    """Class representing a downstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, pool_kernel_size, pool_stride):
        super(DownStep, self).__init__()
        self.step1 = Step(in_channels, out_channels, kernel_size, padding, stride)
        self.step2 = Step(out_channels, out_channels, kernel_size, padding, stride)
        self.maxPool = nn.MaxPool2d(kernel_size=pool_kernel_size, stride=pool_stride)

    def forward(self, batch):
        batch = self.step1(batch)
        batch = self.step2(batch)
            
        batch_to_crop = batch

        batch = self.maxPool(batch)

        return batch, batch_to_crop


class UpStep(nn.Module):
    """Class representing a upstep, according to the original unet implementation."""
    def __init__(self, in_channels, out_channels, kernel_size, padding, stride, upscale_size):
        super(UpStep, self).__init__()
        self.step1 = Step(in_channels, out_channels, kernel_size, padding, stride)
        self.step2 = Step(out_channels, out_channels, kernel_size, padding, stride)
        self.upscale_size = upscale_size


    def crop(self, input, batch_to_crop):
        """
        Function to crop the batch to crop 
        to the size of the input coming from a step below.
        Returns:
            The cropped batch
        """
        cH = (input.size()[2] - batch_to_crop.size()[2]) // 2
        cW = (input.size()[3] - batch_to_crop.size()[3]) // 2

        return torchFunc.pad(batch_to_crop, (-cW, -cW, -cH, -cH))

    def forward(self, batch, batch_to_crop):
        batch = torchFunc.interpolate(batch, self.upscale_size)
        batch = self.step1(batch)
        cropped_batch = self.crop(batch, batch_to_crop)
        batch = torch.cat((batch, cropped_batch), 1)

        batch = self.step1(batch)
        batch = self.step2(batch)
        
        return batch


class Unet(nn.Module):
    """Class that puts all necessary steps for a unet together."""
    def __init__(self, channels, height, width):
        # TODO: Make Unet structure dependent from channels input
        super(Unet, self).__init__()

        kSize = (3,3)
        pad = 1  # padding 1 so ksize 3 doesn't shorten the output
        stride = 1
        pool_kSize = (2,2)
        pool_stride = 2
        
        self.downStep1 = DownStep(channels, 64, kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride)
        self.downStep2 = DownStep(64, 128, kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride)
        self.downStep3 = DownStep(128, 256, kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride)
        self.downStep4 = DownStep(256, 512, kernel_size=kSize, padding=pad, stride=stride, pool_kernel_size=pool_kSize, pool_stride=pool_stride)

        self.center = nn.Sequential(
            Step(512, 1024, kernel_size=kSize, padding=pad, stride=stride),
            Step(1024, 1024, kernel_size=kSize, padding=pad, stride=stride)
        )

        self.upStep1 = UpStep(1024, 512, kernel_size=kSize, padding=pad, stride=stride, upscale_size=(40,40))
        self.upStep2 = UpStep(512, 256, kernel_size=kSize, padding=pad, stride=stride, upscale_size=(80,80))
        self.upStep3 = UpStep(256, 128, kernel_size=kSize, padding=pad, stride=stride, upscale_size=(160,160))
        self.upStep4 = UpStep(128, 64, kernel_size=kSize, padding=pad, stride=stride, upscale_size=(320,320))

        self.output_mask = nn.Conv2d(64, 1, kernel_size=(1,1), padding=0, stride=1)

    def forward(self, batch):
        batch, batch_to_crop1 = self.downStep1(batch)
        batch, batch_to_crop2 = self.downStep2(batch)
        batch, batch_to_crop3 = self.downStep3(batch)
        batch, batch_to_crop4 = self.downStep4(batch)
        
        batch = self.center(batch)


        batch = self.upStep1(batch, batch_to_crop4)
        batch = self.upStep2(batch, batch_to_crop3)
        batch = self.upStep3(batch, batch_to_crop2)
        batch = self.upStep4(batch, batch_to_crop1)
        
        out = self.output_mask(batch)
        #out = torch.squeeze(out, dim=1)  # remove the channel dimenson
        return out
