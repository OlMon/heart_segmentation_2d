import torch
import torch.nn as nn
from torchvision import transforms
from PIL import Image
import torch.utils.data as torchData
import numpy as np


class Heart2DSegmenter:
    """Class that gets the net and implements the training and validation."""
    def __init__(self, net, dataset):
        self.net = net
        self.dataset = dataset

    def _prepare_dataset(self, batch_size=1, validation_split=0.2):
        """Creates the dataloaders and splits the dataset to a training set and validation set."""
        dataset_size = len(self.dataset)
        validation_size = int(np.floor(validation_split * dataset_size))
        train_size = dataset_size - validation_size

        train_dataset, validation_dataset = torchData.random_split(self.dataset, [train_size, validation_size])

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size)
        validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=batch_size)

        return train_dataset, validation_dataset, train_loader, validation_loader
        

    def _calc_epoch(self, train_loader, criterion, optimizer, threshold):
        """Function to calculate one whole epoch."""
        loss_value = 0.0
        for idx, (mini_batch, target) in enumerate(train_loader):
            optimizer.zero_grad()

            outputs = self.net.forward(mini_batch)
            loss = criterion(outputs, target)
            loss.sum().backward() # need to do a sum, because of the many losses from the multiple gpu
            optimizer.step()

            loss_value = loss.sum().item() # need to do a sum, because of the many losses from the multiple gpu
            
            if(idx % 10 == 9):
                print('[Batch %d] loss: %.5f' % (idx + 1, loss_value))
            
    
    def train(self, epochs, criterion, optimizer, threshold=0.5, batch_size=1, validation_split=0.2):
        """Function to train the model.
           Args:
               epochs(int): How many epoch should be trained.
               optimizer(pytorch.optimizer): Optimizer to use during training.
               threshold(float): Threshold to accept a value to a class.
               batch_size(int): What batch size to use during training.
               validation_split(float): How much of the dataset use for validation."""

        self.train_dataset, self.validation_dataset, self.train_loader, self.validation_loader = self._prepare_dataset(batch_size, validation_split)
        self.threshold = threshold
        self.net.train()

        for epoch in range(epochs):
            print('Starting %d. Epoch' % (epoch + 1))
            self._calc_epoch(self.train_loader, criterion, optimizer, threshold)

        print('Finished Training')

        

    def validate(self, threshold=0.5):
        """Function to validate the net.
           Args:
               threshold(float): Threshold to use during the validation."""
        self.net.eval()
        false_pos = 0
        false_neg = 0
        total_pix = 0
        gen_false = 0
        to_predict_ones = 0
        predicted_ones = 0
        make_img=3
        topil = transforms.ToPILImage()
        with torch.no_grad():
            for idx, (scan, mask) in enumerate(self.validation_loader):
                output = self.net.forward(scan)
                pred_mask = (output > threshold).float()
                error_mask = torch.sub(mask, pred_mask.cpu()) # return putput to cpu to be able to calculate with it
                
                if(make_img > 0):
                    topil(mask[0].type(torch.uint8)).save('out/'+str(make_img)+'_mask.png')
                    topil(pred_mask[0].cpu().type(torch.uint8)).save('out/'+str(make_img)+'_pred.png')
                    make_img -= 1
                
                gen_false += len(error_mask.nonzero())
                to_predict_ones += len(mask.nonzero())
                predicted_ones += len(pred_mask.cpu().nonzero())
                false_pos += len((error_mask == -1).nonzero())
                false_neg += len((error_mask ==  1).nonzero())
                total_pix += mask.numel()

        print('Validation Finished')
        print('Result:')
        print('Total pixel predicted: %d' % (total_pix))
        print('Total wrong pixel predicted: %d' % (gen_false))
        print('Total to predicted ones: %d' % (to_predict_ones))
        print('Total predicted ones: %d' % (predicted_ones))
        print('False positive pixels: %d %.2f%%' % (false_pos, false_pos*100/total_pix))
        print('False negative pixels: %d %.2f%%' % (false_neg, false_neg*100/to_predict_ones))
