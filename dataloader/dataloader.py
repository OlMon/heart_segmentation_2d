import glob
import torch
from PIL import Image
import torch.utils.data as data
import numpy as np

class Heart2DSegmentationDataset(data.Dataset):
    """Dataset for heartsegmentation in 2d"""

    def __init__(self, root_dir, endo_or_epi, transform=None):
        """
        Args:
            root_dir(string): Path to folders pantient data, ending with backslash
            endo_or_epi(string): What to load, ENDO or EPI files
            transform(callable, optional): Optional tranformation
        """
        self.root_dir = root_dir
        self.endo_or_epi = endo_or_epi
        self.masks = glob.glob(root_dir + '**/*' + endo_or_epi + '*.png', recursive=True) 
        self.transform = transform

    def __len__(self):
        """Returns the number of data in the dataloader."""
        return len(self.masks)

    def __getitem__(self, idx):
        """
        Gets the item by its index.
        Args:
            idx(int): Index of item to get."""
        mask_dir = self.masks[idx]
        scan_dir = self.masks[idx].replace(self.endo_or_epi, 'ORIG')
        mask = Image.open(mask_dir)
        scan = Image.open(scan_dir)

        if(self.transform):
            scan = self.transform(scan)
            mask = self.transform(mask)

        return (scan.type(torch.FloatTensor), mask.type(torch.FloatTensor))
